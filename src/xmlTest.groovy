
    import com.xlson.groovycsv.CsvParser
    import groovy.util.slurpersupport.GPathResult
    import groovy.xml.MarkupBuilder
    import groovy.xml.XmlUtil
    import org.w3c.dom.Element
    import com.opencsv.*

    /**
     * Created by EKR on 11/30/2015.
     */

    //leggo il file
    def separator='__'
    def ekb_piatto = new XmlSlurper().parse("..\\ekb.xml")

    assert ekb_piatto instanceof groovy.util.slurpersupport.GPathResult

    def tabelle = ekb_piatto
            .depthFirst()
            .findAll { node -> node.name() == 'tabella'  }

    tabelle.eachWithIndex{ def tabella, int i ->
        //creo il file
        FileWriter file = new FileWriter("..\\csv\\Tabella" + i + ".csv")

        def celle = tabella.'*'
                .findAll { node -> node.name() == 'riga' }
                .each { riga ->
                    riga.'*'.findAll { node -> node.name() == 'cella' }
                            .each { cella ->
                                    cella.'**'.findAll{ componente -> componente.name()=='componente'}
                                    .each { componente ->
                                        def spaziatore = componente.'**'.findAll { spaziatore -> spaziatore.name() == 'spaziatore' }*.@tipo
                                        def subTable = componente.'**'.find { subTabella -> subTabella.name() == 'tabella' }*.@id
                                        def blocco_testi = componente.'**'.findAll { blocco -> blocco.name() == 'blocco_testo' }
                                        def testo=[]
                                        def icona=[]
                                        def immagineinline=[]
                                        blocco_testi.each { testi ->
                                            testo.add(testi.paragrafo.elemento.testo.UNI)
                                            icona.add(testi.paragrafo.elemento.icona)
                                            immagineinline.add(testi.paragrafo.elemento.immagine_inline.file.@href)
                                        }

                                        if (subTable != null && subTable.size() > 0)
                                            file.write(subTable.toString())
                                        else {

                                            file.write(("TESTO="+testo.toString()+"!!ICONA="+icona.toString()+"!!IMMAGINE="+immagineinline.toString())
                                                    .normalize()
                                                    .trim()
                                                    .replaceAll("( )+", " ")
                                                    .replaceAll('\n ', separator)


                                            )
                                        }

                                    }
                                    file.write(';')
                            }
                    file.write(System.getProperty('line.separator'))
                }
            file.close()
    }



    /*

                    def value = celle
                    .each { cella ->

                    def paragrafo = it.'*'.'*'.'*'.'*'.findAll { it.name() == 'paragrafo' }
                     paragrafo.each {
                    if (it.elemento.testo.UNI.text())
                        value.add(it.elemento.testo.UNI)
                    }
                        def paragrafo2 = it.'*'.'*'.'*'.'*'.'*'.'*'.'*'.findAll { it.name() == 'paragrafo' }
                paragrafo2.each {
                    if (it.elemento.testo.UNI.text())
                        value.add(it.elemento.testo.UNI)
                }
            }
            //value=(String[])value
            return value
        }
     */